package config;

import bean.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("java")
public class RootConfig {

    @Bean
    public User renat(){
        return new User(0l, "Renat", "renat-s@tut.by");
    }
}
