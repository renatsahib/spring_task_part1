package repository;

import bean.User;

public interface UserRepository {

    User save(User user);
    void remove(long id);
    User getById(long id);
    User getUserByIdEmail( String email);
    User getAll();
}
