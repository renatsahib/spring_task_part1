import bean.User;
import config.RootConfig;
import javafx.application.Application;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import repository.UserRepository;
import repository.UserRepositoryImpl;


public class Runner {
    private static ApplicationContext APPLICATION_CONTEXT = new AnnotationConfigApplicationContext(RootConfig.class);

    public static void main(String[] args) {
        User userRepository = (User) APPLICATION_CONTEXT.getBean("renat");
        System.out.println(userRepository);
    }
}
